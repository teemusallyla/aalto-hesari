chrome.browserAction.onClicked.addListener(function(tab){
	const proxy_url = "https://www-hs-fi.libproxy.aalto.fi/";
	function updateListener(tabId, changeinfo, ntab){
		if (ntab.id == tab.id && ntab.url == proxy_url){
			chrome.tabs.onUpdated.removeListener(updateListener);
			chrome.tabs.update(ntab.id, {"url": redirect});
			chrome.storage.local.set({"hs_last_login": new Date().toJSON()});
		}
	};
	const login_url = "http://libproxy.aalto.fi/login?url=https://www.hs.fi/yrityskirjautuminen/?destination=%2Fpaivanlehti%2F";
	var url = tab.url;
	var redirect = url.replace("www.hs.fi", "www-hs-fi.libproxy.aalto.fi");
	redirect = redirect.replace("dynamic.hs.fi", "dynamic-hs-fi.libproxy.aalto.fi");
	if (redirect != url){
		chrome.storage.local.get("hs_last_login", function(res) {
			var empty = Object.keys(res).length == 0;
			if (empty || new Date() - new Date(res["hs_last_login"]) > 30*60*1000) {
				chrome.tabs.update(tab.id, {"url": login_url});
				chrome.tabs.onUpdated.addListener(updateListener);
			} else {
				chrome.tabs.update(tab.id, {"url": redirect});
			}
		})
	}
});